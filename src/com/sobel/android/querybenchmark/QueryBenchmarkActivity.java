/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sobel.android.querybenchmark;




import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Random;
import java.util.regex.Pattern;

import android.app.Activity;
import android.content.Context;

import com.sobel.android.attestedhttpservice.AttestedPost;
import com.sobel.android.attestedhttpservice.AttestedPostResponse;
import com.sobel.android.hiddenbufferservice.HiddenBufferHandle;
import com.sobel.android.hiddenbufferservice.HiddenBufferManager;
import com.sobel.android.hiddenbufferservice.HiddenContentHandle;
import com.sobel.android.tpmsigningservice.TPMSigningClient;
import com.sobel.jebpf.EBPFInstruction;
import com.sobel.jebpf.EBPFInstruction.Register;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.text.method.ScrollingMovementMethod;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Activity For Application Attested HTTP
 */
public class QueryBenchmarkActivity extends Activity {
    /**
     * Called with the activity is first created.
     */
	
	private static final String TAG = "QueryBenchmark";

	private TextView mText;
	private Button mButton;
	
	// http://www.regular-expressions.info/email.html
	private static Pattern emailRegex = Pattern.compile(
			"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$",
			Pattern.CASE_INSENSITIVE
	);
	
	private static int NSAMPLES = 1000;
	
	private Random mRandom;
	private HiddenBufferManager mHiddenBufferManager;
	
	private TextView mExperimentText;
	private HiddenBufferHandle mHandle;
	
	private int mIntSink; // a place to put otherwise unused variables
	private boolean mBoolSink;
	
	private int mCurrent;
	private Benchmark[] mBenchmarks;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set the layout for this activity.  You can find it
        // in res/layout/hello_activity.xml
        View view = getLayoutInflater().inflate(R.layout.query_benchmark_activity, null);
        setContentView(view);

        mText = (TextView) findViewById(R.id.text);
        mText.setMovementMethod(new ScrollingMovementMethod());
        
        mButton = (Button) findViewById(R.id.button);
        
        mHiddenBufferManager = new HiddenBufferManager();
        setupBenchmark();
    }
    
    public void handleClick(View v) {
    	try {
			doNextBenchmark();
		} catch (IOException e) {
			logToScreen("IOException: " + e.getMessage());
		}
    }
    
    private void setupBenchmark() {
    	// create the hidden buffer.
    	logToScreen("Creating hidden buffer...");
    	
    	// TODO: debug flag in hidden buffer service needs to be set for this to work
    	// (otherwise, creation is restricted to system processes)
    	mHandle = mHiddenBufferManager.newBuffer();


    	logToScreen("Creating non-attached textview...");
    	mExperimentText = new TextView(getApplicationContext());

    	mRandom = new Random();
   
    	mBenchmarks = new Benchmark[] {
    		new LengthBenchmark(),
    		new RegexBenchmark(),
    		new EbpfLuhnBenchmark(),
    	};
    	mCurrent = 0;
    }
    
    private String getRandomASCIIString() {
    	// return random alphanumerosymbolic string

    	// Pick length in [0, 40]
    	int length = mRandom.nextInt(41);

    	char[] chars = new char[length];
    	int i;
    	for (i=0; i<length;i++) {
    		chars[i] = (char)(mRandom.nextInt((0x7E - 0x20) + 1) + 0x20);
    	}
    	
    	return new String(chars);
    }
    
    private void setBothText(String s) {
    	mExperimentText.setText(s);
    	mHiddenBufferManager.setBufferText(mHandle.mPrivateToken, s);
    }
    
    private class LengthBenchmark implements Benchmark {

		@Override
		public String tag() {
			return "LENGTH";
		}

		@Override
		public long local() {
	    	long start = System.nanoTime();
	    	int result = mExperimentText.getText().length();
	    	long end = System.nanoTime();
	    	mIntSink += result;
	    	return (end - start);
		}

		@Override
		public long remote() {
	    	long start = System.nanoTime();
	    	IBinder snapshotToken = mHiddenBufferManager.createSnapshot(mHandle.mPublicToken);
	    	HiddenContentHandle h = new HiddenContentHandle(snapshotToken);
	    	int result = h.getLength();
	    	long end = System.nanoTime();
	    	mHiddenBufferManager.freeSnapshot(snapshotToken);
	    	mIntSink += result;
	    	return (end - start);
		}

    }
    
    private class RegexBenchmark implements Benchmark {

		@Override
		public String tag() {
			return "REGEX";
		}

		@Override
		public long local() {
	    	long start = System.nanoTime();
	    	boolean result = emailRegex.matcher(mExperimentText.getText()).matches();
	    	long end = System.nanoTime();
	    	mBoolSink &= result;
	    	return (end - start);
		}

		@Override
		public long remote() {
	    	long start = System.nanoTime();
	    	IBinder snapshotToken = mHiddenBufferManager.createSnapshot(mHandle.mPublicToken);
	    	HiddenContentHandle h = new HiddenContentHandle(snapshotToken);
	    	boolean result = h.matchesPattern(emailRegex);
	    	long end = System.nanoTime();
	    	mHiddenBufferManager.freeSnapshot(snapshotToken);
	    	mBoolSink &= result;
	    	return (end - start);
		}
    	
    }
    
    private class EbpfLuhnBenchmark implements Benchmark {

		@Override
		public String tag() {
			return "EBPFLUHN";
		}

		private boolean luhnCheck(CharSequence number) {
			/* Adapted from
			 * https://github.com/stripe/stripe-android/blob/2cc84db024a284bca0cfee9bd77a0976f9ee48c9/stripe/src/main/java/com/stripe/android/model/Card.java#L244
			 */
			boolean isOdd = true; // starts true because toggled right away
	        int sum = 0;

	        for (int index = number.length() - 1; index >= 0; index--) {
	            char c = number.charAt(index);
	            if (!Character.isDigit(c)) {
	                continue;
	            }
	            int digitInteger = Integer.parseInt("" + c);
	            isOdd = !isOdd;

	            if (isOdd) {
	                digitInteger *= 2;
	            }

	            if (digitInteger > 9) {
	                digitInteger -= 9;
	            }

	            sum += digitInteger;
	        }

	        return sum % 10 == 0;
		}
		
		@Override
		public long local() {
	    	long start = System.nanoTime();
	    	boolean result = luhnCheck(mExperimentText.getText());
	    	long end = System.nanoTime();
	    	mBoolSink &= result;
	    	return (end - start);
		}

		@Override
		public long remote() {
	    	long start = System.nanoTime();
	    	IBinder snapshotToken = mHiddenBufferManager.createSnapshot(mHandle.mPublicToken);
	    	HiddenContentHandle h = new HiddenContentHandle(snapshotToken);
	    	boolean result = h.runEBPF(EBPFLuhn.codeBytes) == 1;
	    	long end = System.nanoTime();
	    	mHiddenBufferManager.freeSnapshot(snapshotToken);
	    	mBoolSink &= result;
	    	return (end - start);
		}
    	
    }

    private class BenchmarkRunner extends AsyncTask<Benchmark, Integer, Void> {

		@Override
		protected Void doInBackground(Benchmark... params) {
			try {
				doBenchmark(params[0]);
			} catch (IOException e) {
				logToScreen("IOException: " + e.getMessage());
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void r) {
			logToScreen("...DONE");
			mButton.setEnabled(true);
		}
    	
    }
    
    private void doNextBenchmark() throws IOException {
    	// N times DO:
    	// - random string
    	// - time for local
    	// - time for remote
    	//mButton.setEnabled(false);
    	Benchmark b = mBenchmarks[mCurrent];
    	mCurrent++;
    	mCurrent = mCurrent % mBenchmarks.length;
    	doBenchmark(b);
    	
    
    	if (mCurrent == -1) {
    		// Trick compiler to prevent getting rid of unused
    		logToScreen("" + mIntSink + " " + mBoolSink);
    	}

    }

    private interface Benchmark {
    	public String tag();
    	public long local();
    	public long remote();
    }
    
    private void doBenchmark(Benchmark b) throws IOException {
    	OutputStream outputStream;
    	String tag = b.tag();
    	logToScreen(tag);
    	outputStream = openFileOutput(tag + "_log", Context.MODE_PRIVATE);
    	long localLength;
    	long remoteLength;
    	
    	String fmt = "%s|%s|%d|%d\n";
    	int i;
    	for (i=0;i<NSAMPLES;i++) {
    		String s = getRandomASCIIString();
    		setBothText(s);
    		localLength = b.local();
    		remoteLength = b.remote();
    		outputStream.write(String.format(fmt, tag, "LOCAL", s.length(), localLength).getBytes());
    		outputStream.write(String.format(fmt, tag, "REMOTE", s.length(), remoteLength).getBytes());
    	}
  		outputStream.close();
  		logToScreen("... done");
    }
       
    private void logBothPlaces(String s) {
    	logToScreen(s);
    	Log.v(TAG, s);
    }
    
    /**
     * Adds newline and prints to screen
     */
    private void logToScreen(String s) {
    	// Check if we're at the bottom
    	mText.setText(mText.getText() + "*** " + s + "\n");
    	// Scroll it to the bottom if we're
    }
    
}

