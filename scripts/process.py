"""
Does some processing...

<BENCHMARK_TYPE>|<LOCAL|REMOTE>|<string length>|<nanos>
"""

import sys
import os

import statistics
import matplotlib.pyplot as plt

filename = sys.argv[1]
path, ext = os.path.splitext(filename)
print path, ext

local_path = path + "_LOCAL" + ext
remote_path = path + "_REMOTE" + ext
avg_by_length_path = path + "_AVERAGE_BY_LENGTH" + ".png"
avg_path = path + "_AVERAGE" + ext

print "Splitting..."

BENCHMARK_TYPE = None

with open(local_path, 'w') as local:
    with open(remote_path, 'w') as remote:
        with open(filename) as source:
            for line in source:
                parts = line.strip().split('|')

                # Keep track of this...
                if BENCHMARK_TYPE == None:
                    BENCHMARK_TYPE = parts[0]
                else:
                    assert BENCHMARK_TYPE == parts[0]

                if parts[1] == 'LOCAL':
                    local.write(line)
                elif parts[1] == 'REMOTE':
                    remote.write(line)
                else:
                    raise ValueError("Unrecognized: " + line)


print "Removing outliers... (95%)"
for path in (local_path, remote_path):
    data = []
    with open(path) as f:
        for line in f:
            data.append(line.strip().split('|'))

    values = [int(r[3]) for r in data]
    values.sort()

    low_index = int(len(values)*.025)
    high_index = int(len(values)*.975)
    low_threshold = values[low_index]
    high_threshold = values[high_index]

    with open(path, 'w') as f:
        for r in data:
            if low_threshold < int(r[3]) < high_threshold:
                f.write('|'.join(r) + "\n")

print "Getting averages (in u-seconds) by length by type..."
local_average_by_length = {}
remote_average_by_length = {}

averages_by_type = {}
stdev_by_type = {}

average_inputs = [(local_average_by_length, local_path, 'LOCAL'), (remote_average_by_length, remote_path, 'REMOTE')]

for (average_by_length, path, type) in average_inputs:
    samples = []
    count_by_length = {}
    # going to use average_by_length for sum temporarily
    with open(path) as f:
        for line in f:
            parts = line.strip().split('|')
            length = int(parts[2])
            value = int(parts[3])
            count_by_length[length] = count_by_length.setdefault(length, 0) + 1
            average_by_length[length] = average_by_length.setdefault(length, 0) + value
            samples.append(value)
    for length, count in count_by_length.items():
        average_by_length[length] = (average_by_length[length] / float(count)) / 1000 # nano --> mu

    averages_by_type[type] = sum(samples) / float(len(samples))
    stdev_by_type[type] = statistics.stdev(samples)

print "Plotting average by length... (to " + avg_by_length_path + ")"
fig = plt.figure()
ax1 = fig.add_subplot(111)

ax1.scatter(local_average_by_length.keys(), local_average_by_length.values(), s=10, c='b', marker="s", label='Local')
ax1.scatter(remote_average_by_length.keys(), remote_average_by_length.values(), s=10, c='r', marker="o", label='Remote')
ax1.set_autoscale_on(True)
ax1.set_xlim(0, 40)

plt.legend()
plt.title("%s averages by string length by type" % BENCHMARK_TYPE)
fig.savefig(avg_by_length_path)

print "Averages:"
print averages_by_type

with open(avg_path, 'w') as f:
    for k, v in averages_by_type.items():
        f.write("%s|%s|%s\n" % (BENCHMARK_TYPE, k, str(v)))

print "stdev:"
print stdev_by_type

# print "Plotting distributions..."

# fig2 = plt.figure()
# ax2 = fig2.add_subplot(111)

# for path, type in [(local_path, 'LOCAL'), (remote_path, 'REMOTE')]:
#     samples = []
#     with open(path) as f:
#         for line in f:
#             parts = line.strip().split('|')
#             samples.append(int(parts[3]))
#     ax2.hist(samples, bins=1000)

# plt.show()
